const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

export default class DateUtilities {
  static daysBetween(first, second) {

    const one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
    const two = new Date(second.getFullYear(), second.getMonth(), second.getDate());

    const millisecondsPerDay = 1000 * 60 * 60 * 24;
    const millisBetween = two.getTime() - one.getTime();
    const days = millisBetween / millisecondsPerDay;

    return Math.floor(days);
  }

  static longFormat(dateObject) {
    const month = monthNames[dateObject.month - 1];
    const day = dateObject.day + this.getOrdinal(dateObject.day);
    return `${month} ${day} ${dateObject.year}`;
  }

  private static getOrdinal(d) {
    if (d > 3 && d < 21) {
      return 'th';
    }
    switch (d % 10) {
      case 1: return "st";
      case 2: return "nd";
      case 3: return "rd";
      default: return "th";
    }
  }

  static getDateObject(dateString) {
    const date = new Date(dateString);
    return {
      month: date.getMonth() + 1,
      day: date.getDate(),
      year: date.getFullYear()
    };
  }

  static getMonth(mth, short = false) {
    const month = monthNames[mth];
    if (short) {
      return month.slice(0, 3);
    } else {
      return month;
    }
  }

  static addDays(date: Date, days: number) {
    date.setDate(date.getDate() + days);
    return this.formatDate(date);
  }

  static formatDate(date) {
    const d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    const hour = d.getHours();
    const minute = d.getMinutes();
    const second = d.getSeconds();

    return day + "-" + month + "-" + year + " " + hour + ':' + minute + ':' + second;
  }

}
