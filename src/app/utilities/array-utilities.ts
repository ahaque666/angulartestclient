export default class ArrayUtilities {
    static getIds(items, propertyName) {
      if (!items) {
        return;
      }
      const ids = [];
      items.forEach((item) => {
        ids.push(item[propertyName]);
      });
      if (ids.length > 0) {
        return ids;
      }
    }
  
    static uniqBy(arr, predicate) {
      return Object.values(arr.reduce((acc, cur) => Object.assign(acc, { [cur[predicate]]: cur }), {}));
    }
  
    static arraysEqual(arr1, arr2) {
      if (arr1.length !== arr2.length) {
        return false;
      }
      for (let i = arr1.length; i--;) {
        if (arr1[i] !== arr2[i]) {
          return false;
        }
      }
      return true;
    }
  
    static CreateQueryParamStringFromObject(object) {
      return Object.keys(object).map(key => key + '=' + object[key]).join('&');
    }
  
    static arrayMove(array, oldIndex, newIndex) {
      if (newIndex >= array.length) {
        let k = newIndex - array.length + 1;
        while (k--) {
          array.push(undefined);
        }
      }
      array.splice(newIndex, 0, array.splice(oldIndex, 1)[0]);
      return array; // for testing
    }
  
  }
  