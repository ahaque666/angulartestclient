import { Component, Injectable, OnInit } from '@angular/core';
import { SecurityService } from '../services/security.service';
@Injectable({ providedIn: 'root' })
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  constructor(public securityService: SecurityService) { }

  ngOnInit() {
  }

  login(){
    this.securityService.Login();
  }

}
