import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { HomeComponent } from './home/home.component';
import { WeatherforecastComponent } from './weatherforecast/weatherforecast.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ConfigService } from './services/config.service';
import { RouterTestingModule } from '@angular/router/testing';
import { SecurityService } from './services/security.service';
import { StorageService } from './services/storage.service';
import { BasicAuthInterceptor } from './services/auth-interceptor.service';
import { DataService } from './services/data-service';

@NgModule({
  declarations: [
    AppComponent,
    AuthCallbackComponent,
    HomeComponent,
    WeatherforecastComponent,
    LoginComponent
  ],
  imports: [
    RouterTestingModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ConfigService, SecurityService, StorageService, DataService,
  { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
