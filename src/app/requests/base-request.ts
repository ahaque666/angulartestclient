import { Settings } from '../settings/settings';
import { RequestVerbEnum } from '../shared/request-verb';


export class BaseRequest {

    RequestTypeId: number;
    RequestVerb: RequestVerbEnum;
    Endpoint: string;

    constructor(endpoint: string, requestVerb: RequestVerbEnum, requestTypeId?: number) {

        this.RequestTypeId = requestTypeId;
        this.RequestVerb = requestVerb;
        this.Endpoint = endpoint;
    }

    ApiKey: string = Settings.ApiKey();
    CorrelationId: string = Settings.CorrelationId();
    SessionId: string = Settings.SessionId();
}
