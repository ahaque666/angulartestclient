

import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { RequestVerbEnum } from '../shared/constants/request-verb';
import { BaseRequest } from './base-request';
import { MailAdddress } from '../models/MailAddress';
import { SenderEnum } from '../shared/constants/SenderEnum';

class BaseSendEmailRequest extends BaseRequest {
  ToAddresses: MailAdddress[];
  CcAddresses: MailAdddress[];
  BccAddresses: MailAdddress[];
  CustomerCopyAddresses: MailAdddress[];
  PersistenceMessage: Message;
  Sender: SenderEnum;
  ReplyToAddress: MailAdddress;
  Subject: string;
  RecaptchaResponse: string;
  Source: string;
  MoreDetails: string;
  SenderName: string;

  Endpoint: string;
  RequestVerb: RequestVerbEnum;
  RequestTypeId: number;

  constructor(
    endpointUrl: string,
    requestVerb: RequestVerbEnum,
    requestTypeId: number
  ) {
    super(endpointUrl, requestVerb, requestTypeId);

    this.ToAddresses = new Array<MailAdddress>();
    this.CcAddresses = new Array<MailAdddress>();
    this.BccAddresses = new Array<MailAdddress>();
    this.CustomerCopyAddresses = new Array<MailAdddress>();
  }
}

export { BaseSendEmailRequest };