import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SecurityService } from './services/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router, public securityService: SecurityService, private route: ActivatedRoute) { }

  error: boolean;
  subscription: Subscription;
  subscriptionToDestroy: any;

  ngOnInit() {
    this.subscriptionToDestroy = this.route.params.subscribe((params): any => {
      this.securityService.AuthorizedCallback();
    });
  }
  title = 'AngularClient';

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
