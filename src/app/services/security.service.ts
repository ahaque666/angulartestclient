import { Injectable} from '@angular/core';
import { Observable, Subject, timer } from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from './storage.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { EndpointUrls } from '../providers/endpoint-providers';

@Injectable()
export class SecurityService {

    public storage: StorageService;

    constructor(private _http: HttpClient, private _router: Router, private route: ActivatedRoute, private _storageService: StorageService) {
        this.storage = _storageService;
    }  
        
    public IsAuthorized: boolean;
    public IsLoading: boolean;
    public user: any;
    public authenticationSource = new Subject<boolean>();
    public authorityUrl = environment.authorityUrl;

    private authorityClient = environment.authorityClient;
    private authorityScope = environment.authorityScope;
                
    public AuthorizedCallback() {

        if (this.HasAuth() === true){
            if (this.CheckAuthResponse() === true) {
                this.RunTokenValidatation(); 
            } else {
                this.ResetAuthorizationData();
                this._router.navigate(['/']);
            }
        }else{
            //if this is a new tab and there is already a session, find and use it
            if(this.GetToken() && this.GetToken() !== "") {     
                this.SetActiveSession();
                this.RunTokenValidatation();
            } else {
                //single sign on
                //auto log user in if they are logged into the identity server e.g. if they are logged into PM
                this.Login(true, location.pathname);
            }
        }
    }

    private RedirectAfterLogin(){
        var redirectUrl = this.storage.retrieve('redirectUrl');
        this.storage.store('redirectUrl', '');
        if (!redirectUrl)
            this._router.navigate(['/']);
        else{
            this._router.navigate([redirectUrl]);
        }
    }

    public GetToken(): any {
        return this.storage.retrieve('authorizationData');
    }

    private SetActiveSession(){
        this.user = this.storage.retrieve('user');
        this.IsAuthorized = this.storage.retrieve('IsAuthorized');
        this.IsLoading = false;
    }

    private HasAuth(){
        const hash = window.location.hash.substr(1);

        if (hash === ""){
            return false;
        }else{
            return true;
        }
    }

    private CheckAuthResponse(){
        const hash = window.location.hash.substr(1);

        if (hash === ""){
            return;
        }

        const result: any = hash.split('&').reduce(function(result: any, item: string) {
            const parts = item.split('=');
            result[parts[0]] = parts[1];
            return result;
        }, {});

        let token = '';
        let id_token = '';
        let authResponseIsValid = false;

        if (result && !result.error) {

            if (result.state !== this.storage.retrieve('authStateControl')) {
                console.log('AuthorizedCallback incorrect state');
            } else {

                token = result.access_token;
                id_token = result.id_token;

                const dataIdToken: any = this.getDataFromToken(id_token);

                // validate nonce
                if (dataIdToken.nonce !== this.storage.retrieve('authNonce')) {
                    console.log('AuthorizedCallback incorrect nonce');
                } else {
                    this.storage.store('authNonce', '');
                    this.storage.store('authStateControl', '');

                    authResponseIsValid = true;
                    console.log('AuthorizedCallback state and nonce validated, returning access token');
                }
            }
        }

        if (authResponseIsValid) {
            this.IsLoading = true;
            this.SetAuthorizationData(token, id_token, true);
            this.getRefreshToken(result.code);
            return true;
        }
        return false;
    }

    private RunTokenValidatation() {
        let source = timer(3000, 3000).subscribe(() => {
            if (this.IsAuthorized) {
                if(!this.isTokenExpired()) {
                    if (this.isTokenTenMinutesExpired()) {
                        console.log('IsAuthorized: isTokenExpired');
                        source.unsubscribe();
                        this.updateTokens();
                    }
                }else {
                    source.unsubscribe();
                    this.Logoff();
                }
            }
         },
        function (err: any) {
            console.log('Error: ' + err);
        },
        function () {
            console.log('Completed');
        });
    }   

    private SetAuthorizationData(token: any, id_token: any, toRedirectAfterLogin = false) {
        if (this.storage.retrieve('authorizationData') !== '') {
            this.storage.store('authorizationData', '');
        }
        this.storage.store('authorizationData', token);
        this.storage.store('authorizationDataIdToken', id_token);
        this.IsAuthorized = true;
        this.storage.store('IsAuthorized', true);

        this.getUser()
            .subscribe(data => {
                this.user = data.User;
                this.storage.store('user', data.User);
                this.authenticationSource.next(true);
                this.IsLoading = false;
        
                if(toRedirectAfterLogin) {
                    this.RedirectAfterLogin();
                }
            });
    }

    private getUser = (): Observable<any> => {
        return this._http.get(EndpointUrls.UserUrl, {
        }).pipe(map( (res: any) => res));
    }

    public ResetAuthorizationData() {
        this.storage.store('redirectUrl', '');
        this.storage.store('authorizationData', '');
        this.storage.store('authorizationDataIdToken', '');
        this.storage.store('refreshToken', '');
        this.IsAuthorized = false;
        this.storage.store('IsAuthorized', false);
        this.IsLoading = false;
    }

    private isTokenTenMinutesExpired() {
        let user = this.storage.retrieve('user');
        var expiryDate = new Date(user.TokenExpireIn);
        var tenMinsBeforeExpiry = new Date(user.TokenExpireIn);
        tenMinsBeforeExpiry.setMinutes(tenMinsBeforeExpiry.getMinutes() - 10);
        
        if (new Date() < expiryDate && new Date() > tenMinsBeforeExpiry){
            return true;
        }
        return false;
    }

    private isTokenExpired() {
        let user = this.storage.retrieve('user');
        var expiryDate = new Date(user.TokenExpireIn);
        if(new Date() >= expiryDate ) {
            return true;
        }
        return false;
    }

    public CreateAuthorizeUrl(nonce, state, isAutoLogin){
        const authorizationUrl = this.authorityUrl + '/connect/authorize';
        const clientId = this.authorityClient;
        const redirectUri = location.origin + '/openidSignIn';
        const responseType = 'id_token token code';
        const scope = this.authorityScope; 

        this.storage.store('authStateControl', state);
        this.storage.store('authNonce', nonce);

        var url =
            authorizationUrl + '?' +
            'response_type=' + encodeURI(responseType) + '&' +
            'client_id=' + encodeURI(clientId) + '&' +
            'redirect_uri=' + encodeURI(redirectUri) + '&' +
            'scope=' + encodeURI(scope) + '&' +
            'nonce=' + encodeURI(nonce) + '&' +
            'state=' + encodeURI(state)

        if(isAutoLogin) { // this will prevent a user being redirected to the log in page if they're not logged in
            url = url + '&' + 'prompt=none' 
        }

        return url;
    }

    private getRefreshToken(code) {
        const authorizationUrl = this.authorityUrl + '/connect/token';
        const clientId = this.authorityClient;
        const grantTypes = 'authorization_code';
        const redirectUri = location.origin + '/openidSignIn';

          const body = new HttpParams()
            .set('client_id', clientId)
            .set('grant_type', grantTypes)
            .set('code', code)
            .set('redirect_uri', redirectUri);

        this._http.post(authorizationUrl, body.toString(), {
            headers: new HttpHeaders()
              .set('Content-Type', 'application/x-www-form-urlencoded')
          }).subscribe( (res: any) => {
            this.storage.store('refreshToken', res.refresh_token);
        })
    }

    updateTokens() {
        const authorizationUrl = this.authorityUrl + '/connect/token';
        const clientId = this.authorityClient;
        const grantTypes = 'refresh_token';

          const body = new HttpParams()
            .set('client_id', clientId)
            .set('grant_type', grantTypes)
            .set('refresh_token', this.storage.retrieve('refreshToken'));

        this._http.post(authorizationUrl, body.toString(), {
            headers: new HttpHeaders()
              .set('Content-Type', 'application/x-www-form-urlencoded')
          }).subscribe( (res: any) => {
            this.storage.store('refreshToken', res.refresh_token);
            this.updateAuthorizationData(res.access_token, res.id_token).then(() => {
                this.RunTokenValidatation();
            })
        })
    }

    async updateAuthorizationData(token, id_token) {
        return this.SetAuthorizationData(token, id_token);
    }

    private urlBase64Decode(str: string) {
        let output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw new Error('Illegal base64url string!');
        }

        return window.atob(output);
    }

    private getDataFromToken(token: any) {
        let data = {};
        if (typeof token !== 'undefined') {
            const encoded = token.split('.')[1];
            data = JSON.parse(this.urlBase64Decode(encoded));
        }

        return data;
    }

    public Login(isAutoLogin?, redirectUrl?) {
        this.ResetAuthorizationData();
        const nonce = 'N' + Math.random() + '' + Date.now();
        const state = Date.now() + '' + Math.random();

        this.storage.store('authStateControl', state);
        this.storage.store('authNonce', nonce);
        if (redirectUrl) {
            this.storage.store("redirectUrl", redirectUrl);
        }

        var url = this.CreateAuthorizeUrl(nonce, state, isAutoLogin);

        window.location.href = url;
    }

    public Logoff() {
        const authorizationUrl = this.authorityUrl + '/connect/endsession';
        const idTokenHint = this.storage.retrieve('authorizationDataIdToken');
        const postLogoutRedirectUri = location.origin + '/';

        const url =
            authorizationUrl + '?' +
            'id_token_hint=' + encodeURI(idTokenHint) + '&' +
            'post_logout_redirect_uri=' + encodeURI(postLogoutRedirectUri);

        this.ResetAuthorizationData();
        // emit observable
        this.authenticationSource.next(false);
        window.location.href = url;
        this.user = '';
        this.storage.store('user', '');
     }
}
