import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { SecurityService } from '../services/security.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  user: any;
  constructor(private securityService: SecurityService, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot): boolean {
    return this.checkLogin(next);
  }

  checkLogin(next): boolean {
    this.user = this.securityService.user;
    if (this.securityService.IsAuthorized) {
      if (next.data.roles && next.data.roles.indexOf(this.user.Role) === -1) {
        // role not authorised so redirect to home page
        this.router.navigate(['/']);
        return false;
      }
      return true; 
    }
    return false;
  }
}