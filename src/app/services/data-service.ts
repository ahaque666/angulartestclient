import { Injectable } from '@angular/core';
import { BaseRequest } from '../requests/base-request';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import ArrayUtilities from '../utilities/array-utilities';
import { RequestVerbEnum } from '../shared/request-verb';


@Injectable()
export class DataService {

    //public noStore: boolean;

    constructor(
        private http: HttpClient) { }

    addHeaders() {
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        //if (this.noStore && this.noStore === true){
            //headers = headers.append('Cache-Control', 'private, no-store');
        //}
        // if (!this._platformService.isPlatformBrowser()) {
        //     headers.append('x-Mh-Is-Server-Request', 'true');
        // }
        return headers;
    }

    public send<T>(request: BaseRequest): Observable<T> {
        switch (request.RequestVerb) {
            case RequestVerbEnum.Post: {
                return this.post<T>(request);
            }
            case RequestVerbEnum.Get: {
                return this.get<T>(request);
            }
            case RequestVerbEnum.Put: {
                return this.put<T>(request);
            }
            case RequestVerbEnum.Delete: {
                return this.delete<T>(request);
            }
            case RequestVerbEnum.GetUsingIdPattern: {
                return this.getWithUrlIdPattern<T>(request);
            }
            case RequestVerbEnum.DeleteUsingIdPattern: {
                return this.deleteWithUrlIdPattern<T>(request);
            }
            default: {
                return this.post<T>(request);
            }
        }
    }

    public get<T>(request: BaseRequest): Observable<T> {
        return this.http.get<T>(request.Endpoint + '?' + ArrayUtilities.CreateQueryParamStringFromObject(request), {
            headers: this.addHeaders()
        });
    }

    public getWithUrlIdPattern<T>(request: BaseRequest): Observable<T> {
        return this.http.get<T>(request.Endpoint + '?ApiKey=' + request.ApiKey + '&CorrelationId=' + request.CorrelationId + '&SessionId=' + request.SessionId + '&format=json', {
            headers: this.addHeaders()
        });
    }

    public deleteWithUrlIdPattern<T>(request: BaseRequest): Observable<T> {
        return this.http.delete<T>(request.Endpoint + '?ApiKey=' + request.ApiKey + '&CorrelationId=' + request.CorrelationId + '&SessionId=' + request.SessionId + '&format=json', {
            headers: this.addHeaders()
        });
    }

    public post<T>(request: BaseRequest): Observable<T> {
        return this.http.post<T>(request.Endpoint, request, {
            headers: this.addHeaders()
        });
    }

    public put<T>(request: BaseRequest): Observable<T> {
        return this.http.put<T>(request.Endpoint, request, {
            headers: this.addHeaders()
        });
    }

    public delete<T>(request: BaseRequest): Observable<T> {
        return this.http.delete<T>(request.Endpoint + '&ApiKey=' + request.ApiKey + '&CorrelationId=' + request.CorrelationId + '&SessionId=' + request.SessionId, {
            headers: this.addHeaders()
        });
    }

}