import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SecurityService } from './security.service';



@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    constructor(private securityService: SecurityService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with basic auth credentials if available
        let newHeaders = request.headers;
        if (this.securityService.GetToken()) {
            newHeaders = newHeaders.append('Authorization', 'Bearer ' + this.securityService.GetToken());
        }
        const authReq = request.clone({headers: newHeaders});
        return next.handle(authReq);
    }
}