/** 
 * 
 * Holds methods that return near constant values
 * to communicate with the API.
 * 
 * ApiKey() @returns the string api key for the api
 * CorrellationId() @returns a new string GUID for each request.
 * GenerateGuid() @returns the string CorrellationId for @CorrellationId().
 * 
 * */
 export module Settings {

    export function ApiKey() {
        return window.innerWidth >= 992 ? '5f4bc74f-8d9a-41cb-ab85-a1b7cfc86622' : '4284149e-13da-4f12-aed7-0d644a0b7adb';;
    }

    export function CorrelationId() {
        return GenerateGuid();
    }

    export function SessionId() {
        if (window.localStorage && window.localStorage.getItem('userDetails')) {
            return JSON.parse(window.localStorage.getItem('userDetails')).SessionId;
        } else {
            return null;
        }
    }

    export function GenerateGuid() {

        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });

    }
}
