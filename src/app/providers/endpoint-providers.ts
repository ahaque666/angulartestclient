import { environment } from 'src/environments/environment';

export const EndpointUrls = {
    LoginUrl: environment.apiUrl + "/user/login",
    LogoutUrl: environment.apiUrl + "/user/logout",
    LocalitiesUrl: environment.apiUrl + "/localities/",
    Brochure: environment.apiUrl + "/agentbrochure",
    SearchProperty: environment.apiUrl + "/search",
    ContactUsUrl: environment.apiUrl + "/message/contactus",
    PowerBiUrl: environment.apiUrl + "/group/token",
    UserUrl: environment.apiUrl + "user",
    GroupGuidUrl: environment.apiUrl + "/group/guid",
    ViewVirtualViewings: environment.apiUrl + "/property/virtualviewings",
    CreateVirtualViewing: environment.apiUrl + "/property/virtualviewing",
    GetVirtualViewingMessages: environment.apiUrl + "/message/featuredspotmessageslist"

};
