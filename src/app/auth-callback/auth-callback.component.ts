import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SecurityService } from '../services/security.service';

@Component({
  selector: 'app-auth-callback',
  templateUrl: './auth-callback.component.html',
  styleUrls: ['./auth-callback.component.css']
})
export class AuthCallbackComponent implements OnInit, OnDestroy {

  error: boolean;
  subscription: Subscription;
  subscriptionToDestroy: any;

  constructor(public securityService: SecurityService, private router: Router, private route: ActivatedRoute) {}

  async ngOnInit() {
    this.subscriptionToDestroy = this.route.params.subscribe((params): any => {
      this.securityService.AuthorizedCallback();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
