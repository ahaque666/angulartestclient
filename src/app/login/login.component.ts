import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data-service';
import { SecurityService } from '../services/security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit { 

  constructor(
    private router: Router,
    private _dataService: DataService,
    public securityService: SecurityService) { }    
  
    title = "Login";  

    ngOnInit() {
      if (this.securityService.IsAuthorized) {
        this.router.navigate(['/', 'home']);
      }
    }

    login() {
      this.securityService.Login();
    }
}

