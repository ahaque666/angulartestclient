export const enum RequestVerbEnum {
    Post = "POST",
    Get = "GET",
    Put = "PUT",
    Delete = "DELETE",
    DeleteUsingIdPattern = "DELETE_USING_ID_PATTERN",
    GetUsingIdPattern = "GET_USING_ID_PATTERN"
}