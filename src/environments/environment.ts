// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  staging: true,
  testing: false,
  envName: 'staging',
  apiUrl: 'https://stagingapi.myhome.ie',  
  myhomeApiUrl:'https://stagingapi.myhome.ie',
  domainUrl: 'http://localhost:4200',
  authorityUrl: 'https://s-auth.myhome.ie',
  authorityClient: 'jsclient_offr',
  authorityScope: 'openid profile email',
  protocol: 'https',
  agora: {
    appId: '5bee61d01e734aca808939932ff6b1cc'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
